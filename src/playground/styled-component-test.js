import React, { Fragment } from 'react';
import styled, { injectGlobal, keyframes, css, ThemeProvider } from 'styled-components';
import theme from '../../theme';

injectGlobal`
  body {
    padding : 0;
    margin : 0;
  }
`;

const cardCss = css`
  box-shadow : 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);
  background-color : white;
  border-radius : 10px;
  padding : 20px;
`;

const Container = styled.div`
  display : flex;
  flex-direction : column;
  justify-content : space-evenly;
  height : 100vh;
  width : 100%;
  background-color : #bdc3c7;
`;

const mixinButtonSuccess = () => {
    return `
        background-color : #2ecc71;
        font-weight : 100;
    `;
};

const mixinButtonDanger = () => `
    background-color : #e74c3c;
    font-weight : bold;
`;

const Button = styled.button`
    border-radius : 50px;
    padding : 5px;
    min-width : 120px;
    color : white;
    background-color : ${props => props.theme.dark.normalColor};
    -webkit-appearance : none;
    cursor : pointer;
    
    font-weight : 600;
    font-size : 1.5rem;
    
    &:focus, &:active {
        outline : none;
        background-color : white;
    }
        
    ${props => props.success ? mixinButtonSuccess() : ''}
    ${props => props.danger ? mixinButtonDanger() : ''}
    ${props => props.rot ? `animation : ${rotation} ${props.rotationTime}s linear infinite` : ''}
`;

const Anchor = Button.withComponent('a').extend`
  text-decoration : none;
`;

const rotation = keyframes`
    from {
        transform : rotate(0deg);
    }
    
    to {
        transform : rotate(360deg);
    }
`;

const Input = styled.input.attrs({

})`
  width : 200px;
  height : 40px;
  ${cardCss}
`;

const Card = styled.div`
  background-color : red;
`;

const Form = () => (
    <Card>
        <Button>Theme applied</Button>
    </Card>
);

export default () => {
    console.log(<Fragment></Fragment>);
    // console.log(<Fragment />);
    return (
        <ThemeProvider theme={theme}>
            <Container>
                <Button rot rotationTime={5}>normal</Button>
                <Button success>success</Button>
                <Button danger>danger</Button>
                <Anchor danger href="/posts">This is anchor</Anchor>
                <Input placeholder="hello" required />
                <Form />
            </Container>
        </ThemeProvider>
    );
};