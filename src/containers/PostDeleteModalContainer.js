import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import { PostDeleteModal } from '../components/molecules';
import { getPostById, deletePost } from "../modules/post";
import { closePostDeleteModal } from "../modules/ui";
import { MarkdownViewer } from '../components/atoms';

const PostData = styled.div`
  width : 100%;
`;

const MarkdownViewerWrapper = styled.div`
  border : 2px dotted gray;
  width : 100%;
`;

const TitleWrapper = styled.div`
  text-align : left;
  width : 100%;
  border : 5px solid green;
  margin-bottom : 1rem;
`;

class PostDeleteModalContainer extends Component {

    render() {
        const {
            modalIsOpen,
            onCloseModal,
            selectedPostData,
            onDelete,
            history
        } = this.props;

        return (
            <PostDeleteModal
                modalIsOpen={modalIsOpen}
                onClose={onCloseModal}
                onSubmitClick={() => {
                    onCloseModal();
                    onDelete(selectedPostData.id, history);
                }}
                submitText="Delete"
            >
                {
                    selectedPostData &&
                        <PostData>
                            <TitleWrapper>
                                <div className="modal-post-title">{selectedPostData.title}</div>
                            </TitleWrapper>
                            <MarkdownViewerWrapper>
                                <MarkdownViewer
                                    source={selectedPostData.content}
                                />
                            </MarkdownViewerWrapper>
                        </PostData>
                }
            </PostDeleteModal>
        );
    }
}

export default compose(
    withRouter,
    connect(
        state => ({
            modalIsOpen : state.ui.modalIsOpen,
            selectedPostData : getPostById(state)
        }),
        dispatch => ({
            onCloseModal : bindActionCreators(closePostDeleteModal, dispatch),
            onDelete : bindActionCreators(deletePost, dispatch)
        })
    )
)(PostDeleteModalContainer);
