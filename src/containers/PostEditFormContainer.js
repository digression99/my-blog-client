import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators, compose} from "redux";
import {withRouter, Redirect} from "react-router-dom";

import {PostForm} from '../components/organisms';
import {submitEditPost} from "../modules/post";
import {isEmptyObject, getTagsString} from "../lib";

const PostEditFormContainer = ({onSubmit, posts, hashtags, match: {params}}) => {
    const handleSubmit = postId => (val, history) => onSubmit(postId, val, history);
    return (
        <div>
            {!isEmptyObject(posts)
                ? <PostForm
                    initialValues={{
                        title: posts[params.id].title,
                        content: posts[params.id].content,
                        hashtags: getTagsString(posts[params.id].hashtags, hashtags)
                    }}
                    onSubmit={handleSubmit(params.id)}
                    pageName="edit"
                />
                : <Redirect to="/"/>
            }
        </div>
    );
};

export default compose(
    connect(
        state => ({
            posts: state.posts.byId,
            hashtags: state.hashtags.byId
        }),
        dispatch => ({
            onSubmit: bindActionCreators(submitEditPost, dispatch)
        })
    ),
    withRouter
)(PostEditFormContainer);
