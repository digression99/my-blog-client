import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import { PostForm } from '../components/organisms';
import { submitCreatePost } from "../modules/post";

const PostCreateFormContainer = ({ onSubmit }) => (
    <PostForm
        initialValues={{}}
        onSubmit={onSubmit}
    />
);

export default connect(
    null,
    dispatch => ({
        onSubmit : bindActionCreators(submitCreatePost, dispatch)
    })
)(PostCreateFormContainer);