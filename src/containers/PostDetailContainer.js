import React, {Component, Fragment} from 'react';
import {compose, bindActionCreators} from 'redux';
import {withRouter} from 'react-router';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import moment from 'moment';

import {getPosts} from '../modules/post';
import {PostDetail} from '../components/organisms';
import {EntryComments} from "../components/molecules";
import {
    openPostDeleteModal,
    selectPost
} from "../modules/ui";

class PostDetailContainer extends Component {
    render() {
        const {posts, match: {params}, selectPost, openPostDeleteModal } = this.props;
        const post = posts ?
            {
                ...posts[params.id],
                dateCreated: moment(posts[params.id].dateCreated).fromNow()
            }
            : null;

        return (
            <Fragment>
                {
                    post
                        ?
                        <Fragment>
                            <PostDetail
                                {...post}
                                onDeleteButtonClick={() => {
                                    selectPost(params.id);
                                    openPostDeleteModal();
                                }}
                            />
                            <EntryComments
                                postId={params.id}
                            />
                        </Fragment>
                        : <Redirect to="/"/>
                }
            </Fragment>
        );
    }
}

export default compose(
    connect(
        state => ({
            posts: getPosts(state)
        }),
        dispatch => ({
            openPostDeleteModal: bindActionCreators(openPostDeleteModal, dispatch),
            selectPost: bindActionCreators(selectPost, dispatch)
        })
    ),
    withRouter
)(PostDetailContainer);
