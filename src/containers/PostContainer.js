import React, {Component} from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetchPosts, filterPostsByHashtagsSelector } from '../modules/post';
import { getHashtags } from "../modules/hashtag";
import { PostList, PostGrid } from '../components/organisms';

class PostContainer extends Component {

    async componentDidMount() {
        await this.props.fetchPosts();
    }

    renderPosts() {
        const { filter, posts, hashtags } = this.props;
        if (filter === 'list') {
            return <PostList posts={posts} hashtags={hashtags} />
        } else if (filter === 'grid') {
            return <PostGrid posts={posts} hashtags={hashtags} />
        }
    }

    render() {
        return (
            <div>
                {this.renderPosts()}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    posts : filterPostsByHashtagsSelector(state),
    hashtags : getHashtags(state),
    filter : state.ui.postDisplay
});

const mapDispatchToProps = dispatch => ({
    fetchPosts : bindActionCreators(fetchPosts, dispatch)
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps)
)(PostContainer);
