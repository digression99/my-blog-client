import {injectGlobal} from "styled-components";

injectGlobal`
  * {
    margin : 0;
    padding : 0;
    border : 0;
    box-sizing : inherit;
  }
  
  html {
    height : 100%;
  }
  
  body {
    font-family : Lato, D2Coding, sans-serif;
    box-sizing : border-box;
    background-color : transparent;
    height : 100%;
  }
  
  #root {
    height : 100%;
  }
`;