
const normal = {
    mainColor : '#3498db',
    dangerColor : '#e74c3c',
    successColor : '#2ecc71',
    normalColor : 'yellow'
};

const dark = {
    mainColor : '#3498db',
    dangerColor : '#e74c3c',
    successColor : '#2ecc71',
    normalColor : 'black'
};

export default { normal, dark };