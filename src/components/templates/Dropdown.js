import React from 'react';
import styled from 'styled-components';

const Dropdown = styled.div`
  display : flex;
  flex-wrap : wrap;
  justify-content : stretch; 
  
  position : absolute;
  top : 100%;
  width : 100%;
  z-index : 1000;
  
  background-color : white;
  //transition : all 1s;
  //min-height : 40rem;
  
  & > * {
    flex : 0 0 initial;
    font-size : 1.5rem;
    font-weight : 300;
    color : gray;
    height : 2.5rem;
  }
`;

export default ({ children, onMouseEnter, onMouseLeave }) => {
    return (
        <Dropdown
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
        >
            {children}
        </Dropdown>
    );
};
