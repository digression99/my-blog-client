import React, {Fragment} from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display : flex;
  flex-direction : column;
  height : 100%;
`;

const HeaderTemplate = styled.div`
  display : flex;
  justify-content : center;
  min-height : 4rem;
  border-bottom : 1px solid grey;
  //margin : 0 auto 1rem auto;
`;

const FooterTemplate = styled.div`
  display : flex;
  justify-content : center;
  //margin : 1rem auto 0 auto;
  min-height : 10rem;
  border-top : 1px solid grey;
`;

const MainTemplate = styled.div` 
  margin : 0 auto;
  min-height : 40rem;
`;

const PageTemplate = ({ header, footer, children }) => {
    return (
        <Fragment>
            <HeaderTemplate>
                {header}
            </HeaderTemplate>
            <MainTemplate>
                {children}
            </MainTemplate>
            <FooterTemplate>
                {footer}
            </FooterTemplate>
        </Fragment>
    );
};

export default PageTemplate;
