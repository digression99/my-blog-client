import React, { Component, Fragment } from 'react';
import styled, { injectGlobal } from 'styled-components';
import Modal from 'react-modal';

// Modal.setAppElement('#root');

const WrapperOne = styled.div`
  display : grid;
  grid-template-columns : repeat(3, 1fr);
  grid-gap : 10px;
  grid-auto-rows : minmax(100px, auto);
  
  .one {
    grid-column : 1/ 3;
    grid-row : 1;
    background-color : rgba(1, 2, 3, 0.3);
  }
  
  .two {
    grid-column : 2 / 4;
    grid-row : 1 / 3;
    background-color : rgba(255, 0, 0, 0.8);
  }
  .three {
      grid-column: 1;
      grid-row: 2 / 5;
      background-color : rgba(0, 255, 0, 0.9);
  }
    .four {
      grid-column: 3;
      grid-row: 3;
      background-color : rgba(0, 0, 255, 0.1);
    }
    .five {
      grid-column: 2;
      grid-row: 4;
    }
    .six {
      grid-column: 3;
      grid-row: 4;
    }
`;

const WrapperTwo = styled.div`
  display : grid;
  //grid-template-columns : 20px repeat(2, 1fr) 20px;
  //grid-template-columns : repeat(5, 1fr 2fr);
  grid-template-columns : repeat(3, 1fr);
  //grid-auto-rows : 200px;
  grid-auto-rows : minmax(auto, 100px);
  overflow : hidden;
  
  
  .one {
    border : 2px solid gray;  
    background-color : indianred;
  }
  .two {
      border : 2px solid gray;
      background-color : blueviolet;
  }
  .three {
    border : 2px solid gray;
    background-color : greenyellow;
  }
  .four {
    border : 2px solid gray;
    background-color : antiquewhite;
  
  }
  .five {
    border : 2px solid gray;
    background-color : darkslateblue;
  
  }
  .six {
    border : 2px solid gray;
    background-color : darkslategray;
  }
`;

const WrapperThree = styled.div`
  display : grid;
  grid-template-columns : repeat(3, 1fr);
  grid-auto-rows : minmax(100px, auto);
  column-gap : 10px;
  row-gap : 1em;
  
  //height : 300px;
  
  .box {
    border : 2px solid gray;
  }
  
  .one {
    display : grid;
    //grid-template-columns : subgrid; // not yet supported.
    grid-auto-rows : minmax(50px, auto);
    grid-column-start : 1;
    grid-column-end : 4;
    grid-row-start : 1;
    grid-row-end : 3;
    grid-row-gap : 5px;
    
    padding : 5px;
    margin : 0;
     
    background-color : indianred;
  }
  .two {
    grid-column-start : 1;
    grid-row-start : 3;
    grid-row-end : 5;
    
    background-color : blueviolet;
  }
  .three {
    background-color : greenyellow;
  }
  .four {
    background-color : antiquewhite;
  }
  .five {
    background-color : darkslateblue;
  }
  .six {
    background-color : darkslategray;
  }
  
  .nested {
    background-color : darkgoldenrod;
  }
`;

const WrapperFour = styled.div`
  display : flex;
  flex-wrap : nowrap;
  justify-content : space-evenly;
  
  & > div {
    flex : 1 1 auto;
    margin : 10px;
  }
`;

const TestComponent1 = () => (
    <WrapperFour>
        <div className="box one">
            <div className="nested">a</div>
            <div className="nested">b</div>
            <div className="nested">c</div>
        </div>
        <div className="box two">two</div>
        <div className="box three">three Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eaque, excepturi itaque laboriosam molestias, necessitatibus nisi non nulla numquam obcaecati odit provident quis tenetur. Aut autem est quas quisquam repellat. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem consequatur distinctio dolores doloribus molestiae pariatur perferendis. Accusamus debitis distinctio, enim error impedit nam odit, perspiciatis provident quis sed veniam voluptate?</div>
        <div className="box four">four</div>
        <div className="box five">five</div>
        <div className="box six">six</div>
    </WrapperFour>
);

const TestButton = styled.button`
  width : 200px;
  height : 40px;
  background-color : red;
`;

const Wrapped = styled(TestButton)`
  background-color : blue;
`;

const TestComponent2 = () => (
    <Fragment>
        <Wrapped>Wrapped button</Wrapped>
        <TestButton>Test button</TestButton>
    </Fragment>
);

injectGlobal`
    .Modal {
        position: absolute;
        top: 40px;
        left: 40px;
        right: 40px;
        bottom: 40px;
        background-color: papayawhip;
        display : flex;
        flex-direction : column;
        justify-content : space-around;
        
        .modal--data-info {
          display : flex;
          flex : 6 1 auto;
          justify-content : center;
          align-items : center;
          
          width : 100%;
          
        }
        
        .modal--button-container {
            display : flex;
            justify-content : space-evenly;
            
            height : 4rem;
            flex : 1 1 auto;
            
            & .modal--button {
              flex : 1 1 auto;
              height : 100%;
              margin : 0.2rem;
              border-radius : 20px;
              font-size : 1.5rem;
              cursor : pointer;
              
              &:hover {
                background-color : red;
                color : white;
              }          
            } 
        }
    }
  
    .Overlay {
        background-color : rgba(0, 0, 0, 0.5);
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }
`;

const TestComponent3Wrapper = styled.div`
  .open-modal-button {
    width : 10rem;
    height : 2rem;
    font-size : 1.5rem;
  }
  
`;

const TestComponent3 = ({ modalIsOpen, onCloseModal, onOpenModal, onDelete }) => (
    <TestComponent3Wrapper>
        <button
            className="open-modal-button"
            onClick={onOpenModal}
        >
            Open
        </button>
        <Modal
            isOpen={modalIsOpen}
            className="Modal"
            overlayClassName="Overlay"
        >
            <div
                className="modal--data-info"
            >
                This is info.
            </div>


            <div className="modal--button-container">
                <button
                    className="modal--button"
                    onClick={onCloseModal}
                >Close modal</button>
                <button
                    className="modal--button"
                    onClick={onDelete}
                >Delete!</button>
            </div>
        </Modal>
    </TestComponent3Wrapper>
);

class TestPage extends Component {

    state = {
        modalIsOpen : false
    };

    openModal() {
        console.log('open modal clicked.');
        this.setState({ modalIsOpen : true });
    }

    closeModal() {
        console.log('close modal clicked.');
        this.setState({ modalIsOpen : false });
    }

    onDelete() {
        console.log('on delete clicked.');
        this.setState({ modalIsOpen : false });
    }

    render() {
        return (
            <div
                style={{
                    width : '200px',
                    height : '400px',
                    backgroundColor : 'red',
                    zIndex:1000
                }}
                onMouseEnter={() => console.log('on mouse enter.')}
                onMouseLeave={() => console.log('on mouse leave.')}
            >
                hi
            </div>
        );
    }
}

export default TestPage;
