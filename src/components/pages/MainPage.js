import React from 'react';
import styled from "styled-components";
import DashboardImage from '../../resources/images/phife-ftrjpg_t2tiyle7jrzi18ciq6r33p78g.jpg';

import { Header, Footer } from '../organisms';
import {PageTemplate} from '../templates';
import { Link } from '../atoms';

const Wrapper = styled.div`
  display : flex;
  flex-direction : column;
  justify-content : center;
  align-items : center;
  color : #fff;
  font-size : 2rem;
  
  background-image: linear-gradient(to right bottom, rgba(126, 213, 111, 0.7), rgba(40, 180, 131, 0.6)), url("${DashboardImage}");
  background-size : cover;
  background-position : top;
  min-height : 100vh;
  
  .header-title {
    margin-bottom : 20px;
    opacity : 1;
  }
  
  .header-button {
    opacity : 1;
  }
`;

export default () => {
    return (
        <div>
            <PageTemplate
                header={<Header />}
                footer={<Footer />}
            >
                <Wrapper>
                    <div
                        className="header-title"
                    >
                        Hashtag Revisited.
                    </div>
                    <Link
                        to="/posts"
                        className="header-button"
                    >Read Posts
                    </Link>
                </Wrapper>
            </PageTemplate>
        </div>
    );
};