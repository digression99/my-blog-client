import React, { Component } from 'react';
// import { Link } from '../atoms';
import { Link } from 'react-router-dom';

import styled from 'styled-components';
import { PostFilter } from '../molecules';
import { HashBox, Header, Footer } from '../organisms';
import { PostContainer } from '../../containers';
import {PageTemplate} from '../templates';

const StyledLink = styled(Link)`
  transition : all 0.2s;
  display : flex;
  justify-content : flex-end;
  text-decoration : none;
  border-radius : 0;
  :link, :visited {
      width : 100%;
      height : 3rem;
      font-size : 2rem;
      border-radius : 0;
      color : black;
  }
  :hover {
    background : linear-gradient(to right, rgba(127, 90, 80, 0.3), white), white;
  }
`;

const Wrapper = styled.div`
  display : flex;
  justify-content : center;
  align-items : center;
`;

const Container = styled.div`
  width : 800px;

`;

class PostPage extends Component {

    render() {
        return (
            <PageTemplate
                header={<Header />}
                footer={<Footer />}
            >
                <Wrapper>
                    <Container>
                        <StyledLink to="/posts/create">
                            Create Post
                        </StyledLink>
                        <PostFilter />
                        <HashBox />
                        <PostContainer />
                    </Container>
                </Wrapper>
            </PageTemplate>
        );

    }
}

export default PostPage;