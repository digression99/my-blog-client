import MainPage from './MainPage';
import PostPage from './PostPage';
import PostDetailPage from './PostDetailPage';
import PostCreatePage from './PostCreatePage';
import PostEditPage from './PostEditPage';
import TestPage from './TestPage';

export {
    PostEditPage,
    MainPage,
    PostPage,
    PostDetailPage,
    PostCreatePage,
    TestPage
};