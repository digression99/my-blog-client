import React from 'react';

import {PageTemplate} from '../templates';
import { Header, Footer } from '../organisms';
import { PostCreateFormContainer } from '../../containers';
import styled from 'styled-components';
import {Link } from 'react-router-dom';

const HeaderWrapper = styled.div`
  display : flex;
  align-items : center;
  .header-link {
    text-decoration : none;
    font-size : 2rem;
    color : black;
  }
`;

const PostCreatePage = () => (
    <PageTemplate
        header={
            <HeaderWrapper>
                <Link to="/" className="header-link">
                    #
                </Link>
            </HeaderWrapper>
        }
        footer={<Footer />}
    >
        <PostCreateFormContainer />
    </PageTemplate>
);

export default PostCreatePage;
