import React from 'react';

import {PageTemplate} from '../templates';
import { Header, Footer } from '../organisms';
import { PostEditFormContainer } from '../../containers';

const PostEditPage = () => {
    return (
        <PageTemplate
            header={<Header/>}
            footer={<Footer/>}
        >
            <PostEditFormContainer />
        </PageTemplate>
    );
};

export default PostEditPage;
