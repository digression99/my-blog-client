import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import {PageTemplate} from '../templates';

import { Header, Footer } from '../organisms';
import {
    PostDetailContainer,
    PostDeleteModalContainer
} from '../../containers';

const Wrapper = styled.div`
    display : flex;
    justify-content : center;
    color : black;
`;

const Container = styled.div`
  width : 800px;
`;

const GoBackLink = styled(Link)`
    flex : 1;
    
    text-decoration : none;
    color : black;
    width : 200px;
    height : 40px;
    font-size : 1.5rem;
    font-weight : bold;
`;

class PostDetailPage extends Component {
    render() {
        return (
            <PageTemplate
                header={<Header />}
                footer={<Footer />}
            >
                <Wrapper>
                    <Container>
                        <div className="button-box">
                            <GoBackLink to="/posts">
                                Go Back
                            </GoBackLink>
                        </div>
                        <PostDetailContainer />
                        <PostDeleteModalContainer />
                    </Container>
                </Wrapper>
            </PageTemplate>
        );
    }
}

export default PostDetailPage;
