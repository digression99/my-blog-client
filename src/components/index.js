import React, {Component, Fragment} from 'react';
import {Helmet} from 'react-helmet';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { ProgressBar } from './organisms';

import Routes from './Routes';

class App extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>My Blog</title>
                </Helmet>
                <Routes />
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <ProgressBar />
            </Fragment>
        );
    }
}

export default App;
