import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Header, Footer } from './organisms';

import {
    MainPage,
    PortfolioPage,
    PostCreatePage,
    PostDetailPage,
    PostEditPage,
    PostPage,
    ProfilePage, TestPage
} from "./pages/index";


const Routes = () => {
    return (
        <Fragment>
            <Switch>
                <Route exact path="/" component={MainPage}/>
                <Route exact path="/posts" component={PostPage}/>
                <Route
                    exact
                    path="/posts/create"
                    component={PostCreatePage}
                />
                <Route
                    path="/posts/edit/:id"
                    component={PostEditPage}
                />
                <Route exact
                       path="/posts/:id"
                       component={PostDetailPage}
                />
                <Route path="/test" component={TestPage}/>
            </Switch>
        </Fragment>
    );
};

export default Routes;
