import React from 'react';
import styled from 'styled-components';
import { darken, lighten } from 'polished';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { selectHashtag, deselectHashtag } from "../../modules/hashtag";
import { Chip } from '../atoms/index';

const StyledHashtagContainer = styled.ul`
  display : grid;
  flex : 1 1 auto;
  grid-template-columns : repeat(auto-fill, minmax(100px, auto));
  justify-content : space-between;
  width : 100%;
`;

const ChipBox = ({
                     hashtags,
                     selectHashtag,
                     isInBox,
                     deselectHashtag
}) => {

    const handleChipClick = hashtag => {
        if (!isInBox) {
            selectHashtag(hashtag)
        } else {
            deselectHashtag(hashtag)
        }
    };

    const renderChips = hashtags => hashtags.map(hashtag =>
        <Chip key={hashtag._id}
              onTagClick={() => handleChipClick(hashtag)}
              hoverstate={isInBox ? "delete" : "add"}
        >
            {`#${hashtag.tag}`}
        </Chip>
    );

    return (
        <StyledHashtagContainer>
            {renderChips(hashtags)}
        </StyledHashtagContainer>
    );
};

const mapDispatchToProps = dispatch => ({
    selectHashtag : bindActionCreators(selectHashtag, dispatch),
    deselectHashtag : bindActionCreators(deselectHashtag, dispatch)
});

export default compose(
    connect(null, mapDispatchToProps)
)(ChipBox);
