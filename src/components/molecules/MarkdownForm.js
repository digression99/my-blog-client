import React from 'react';
import styled from "styled-components";
import { Field } from 'redux-form';

import { CreateMarkdownViewer } from '../atoms';

export const MarkdownWrapper = styled.div`
    display : flex;
    flex-direction : row;
    justify-content : center;
    height : 1000px;
    width : 100%;
    
    .markdown-editor {
      width : 100%;
      font-size : 1.5rem;
    }
`;

const Divider = styled.div`
  width : 3px;
  background-color : black;
  margin : 0 1rem;
`;

const MarkdownForm = ({ source }) => (
    <MarkdownWrapper>
        <Field
            className="markdown-editor"
            key="content"
            name="content"
            type="text"
            component="textarea"
            placeholder="enter markdown text."
            style={{
                resize : 'none'
            }}
        />
        <Divider />
        <CreateMarkdownViewer source={source}/>
    </MarkdownWrapper>
);

export default MarkdownForm;
