import React from 'react';
import moment from "moment";
import styled from 'styled-components';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { darken, lighten } from 'polished';

import ChipBox from './ChipBox';

import {
    openPostDeleteModal,
    selectPost
} from "../../modules/ui";

const Container = styled.div`
  padding : 1rem;
  width : 100%;
  background-color : ${props => props.bgcolor};
  
  &:hover {
    .title, .content, .date-created {
      color : #E80C7A;
    }
  }
  
  .item-footer {
    display : flex;
    
    .button-box {
      display : flex;
      flex : 1 1 auto;
      justify-content : flex-end;
    }
  }
`;

const StyledPostListItem = styled(Link)`
  display : flex;
  flex-direction : column;
  justify-content : space-around;
  text-decoration : none;
  width : 100%;
  height : 100%;
  
  &:visited, &:link {
    color : ${props => props.hovercolor};
    border-radius : 100px;
  }
  
  .content-box {
    flex : 1 1 auto;
  }
  
  .title {
    font-size : 2rem;
    color : ${props => props.hovercolor === "blanchedalmond" ? "white" : "black"};
  }
  
  .content {
    font-size : 1.5rem;
    color : ${props => props.hovercolor === "blanchedalmond" ? darken(0.1,"white") : lighten(0.5,"black")};
  }
  
  .date-created {
    flex : 1 1 auto;
    font-size : 1rem;
    font-weight : bold;
  }
`;

const ListItem = ({
                      id,
                      title,
                      content,
                      dateCreated,
                      tags,
                      bgColor,
                      hoverColor
}) => (
        <Container
            bgcolor={bgColor}
            hovercolor={hoverColor}
        >
            <StyledPostListItem
                to={`/posts/${id}`}
                bgcolor={bgColor}
                hovercolor={hoverColor}
            >
                <div className="content-box">
                    <div className="title">{title}</div>
                    <div className="content">{content}</div>
                </div>
                <div className="date-created">{moment(dateCreated).fromNow()}</div>
            </StyledPostListItem>
            <div
                className="item-footer"
            >
                <ChipBox hashtags={tags} isInBox={false} />
            </div>
        </Container>
);

const mapStateToProps = (state, myProps) => ({

});

const mapDispatchToProps = dispatch => ({
    openPostDeleteModal : bindActionCreators(openPostDeleteModal, dispatch),
    selectPost : bindActionCreators(selectPost, dispatch)
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps)
)(ListItem);