import React, { Component, Fragment } from 'react';
import { CommentCount, DiscussionEmbed } from 'disqus-react';

class EntryComments extends Component {

    render() {
        const { postId } = this.props;

        const disqusShortname = 'pseudocoderkim';
        const disqusConfig = {
            url : `http://localhost:3000/posts/${postId}`,
            identifier : postId,
            title : postId
        };

        return (
            <Fragment>
                <CommentCount shortname={disqusShortname} config={disqusConfig}>
                </CommentCount>
                <DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
            </Fragment>
        );
    }
}

export default EntryComments;