import React, { Component } from 'react';
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import {showListPost, showGridPost} from "../../modules/ui";

const Wrapper = styled.div`
  display : flex;
  justify-content : space-between;
  width : 100%;
`;

const SelectButton = styled.button`
  flex : 1 1 auto;
  font-size : 2rem;
  font-weight : bold;
  height : 4rem;
  transition : all 0.2s ease-in-out;
  
  background-color : ${props => {
      return props.filter === props.targetFilter ? '#dee2e6' : 'white';
  }};
  
  color : ${props => {
      return 'black';
      // return props.filter === props.targetFilter ? 'white' : '#dee2e6';
  }};
  
  &:hover {
    cursor : pointer;
    background-color : #868e96;
    color : white;
    transform : translateY(-3px);
    box-shadow : 0 10px 20px rgba(0, 0, 0, .2); /*{offsetx, offsetdown, blur, color}*/ 
  }
`;

class PostFilter extends Component {

    render() {
        const { showListPost, showGridPost, filter } = this.props;
        return (
            <Wrapper>
                <SelectButton
                    onClick={() => showListPost()}
                    targetFilter="list"
                    filter={filter ? filter : 'list'}
                >List
                </SelectButton>
                <SelectButton
                    onClick={() => showGridPost()}
                    targetFilter="grid"
                    filter={filter ? filter : 'list'}
                >
                    Grid
                </SelectButton>
            </Wrapper>
        );
    }
}

const mapStateToProps = state => ({
    filter : state.ui.postDisplay
});

const mapDispatchToProps = dispatch => ({
    showListPost : bindActionCreators(showListPost, dispatch),
    showGridPost : bindActionCreators(showGridPost, dispatch)
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps)
)(PostFilter);