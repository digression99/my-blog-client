import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

import ChipBox from './ChipBox';

import Link from '../atoms/Link';

const Wrapper = styled.div`
  display : flex;
  flex-direction : column;
  justify-content : center;
  align-items : center;
  padding : 0.5rem;
  border : 0.1rem dotted rgba(10, 230, 46, 0.3);
  
  &:hover {
    .title, .content, .date-created {
      color : #E80C7A;
    }
  }
`;

const StyledLink = styled(Link)` 
  display : flex;
  flex : 1 1 auto;
  flex-direction : column;
  justify-content : space-around;
  text-decoration : none;
  width : 100%;
  height : 100%;
  
  
  &:visited, &:link {
    color : ${props => props.hovercolor};
  }
  
  .content-box {
    flex : 1 1 auto;
    background-color : rgba(50, 10, 230, 0.2);
  }
  
  .title {
    font-size : 2rem;
    background-color : rgba(30, 189, 129, 0.2);
  }
  
  .content {
    font-size : 1.5rem;
  }
  
  .date-created {
    flex : 1 1 auto;
    font-size : 1rem;
    font-weight : bold;
  }
`;

const GridItem = ({ id, title, content, dateCreated, tags }) => {
    return (
        <Wrapper>
            <StyledLink
                to={`/posts/${id}`}
            >
                <div className="content-box">
                    <div className="title">{title}</div>
                    <div className="content">{content}</div>
                </div>
                <div className="date-created">{moment(dateCreated).fromNow()}</div>
            </StyledLink>
            <ChipBox
                hashtags={tags}
                style={{ flex : '1 1 auto' }}
                isInBox={false}
            />
        </Wrapper>
    );
};

export default GridItem;