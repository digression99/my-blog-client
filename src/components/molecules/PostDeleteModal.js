import React from 'react';
import styled, {injectGlobal} from 'styled-components';
import Modal from 'react-modal';
import {Button} from '../atoms';

injectGlobal`
  .Modal {
    position : absolute;
    margin : 0 auto;
    top : 40px;
    left : 40px;
    right : 40px;
    bottom : 40px;
    background-color : papayawhip;
    display : flex;
    flex-direction : column;
    justify-content : space-around;
    max-width : 800px;
  }
  
  .Overlay {
    background-color : rgba(0, 0, 0, 0.5);
    position : fixed;
    top : 0;
    left : 0;
    right : 0;
    bottom : 0;
  }
`;

const ModalDataContainer = styled.div`
  display : flex;
  flex-direction : column;
  justify-content : center;
  align-items : center;
  font-weight : 400;
  font-size: 1.5rem;
  height : 100%;
  
  & * {
    flex : 1 1 auto;
  }
`;

const ModalButtonContainer = styled.div`
  display : flex;
  justify-content : flex-end;
  margin-right : 1rem;
  margin-bottom : 1rem;
`;

const PostDeleteModal = ({
                             modalIsOpen,
                             onClose,
                             onOpenModal,
                             onDelete,
                             onSubmitClick,
                             submitText,
                             children
                         }) => (
    <Modal
        isOpen={modalIsOpen}
        shouldCloseOnOverlayClick={true}
        className="Modal"
        overlayClassName="Overlay"
    >
        <ModalDataContainer>
            {children}
        </ModalDataContainer>
        <ModalButtonContainer>
            <Button
                className="modal-close-button"
                onClick={onClose}
            >
                Close
            </Button>
            <Button
                className="modal-delete-button"
                onClick={onSubmitClick}
            >
                {submitText}
            </Button>
        </ModalButtonContainer>
    </Modal>
);

export default PostDeleteModal;
