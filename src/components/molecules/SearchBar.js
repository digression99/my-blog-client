import React, { Component } from 'react';
import styled from 'styled-components';
import {reduxForm, Field} from 'redux-form';
import {compose, bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Dropdown} from '../templates';

import {searchFormSelector} from '../../modules/form';
import {searchedHashtagsSelector} from "../../modules/ui";
import {Chip} from '../atoms';

import {
    getHashtags,
    searchHashtags,
    selectHashtag
} from "../../modules/hashtag";

const Form = styled.form`
  display : flex;
  flex-direction : column;
  position : relative;
`;

const StyledInput = styled(Field)`
  width : 100%;
  height : 3rem;
  font-size : 1.5rem;
  font-weight : 300;
  color : orangered;
  background : transparent;
  transition : all 0.2s;
  :hover {
    background : linear-gradient(to left, rgba(127, 90, 80, 0.3), white), transparent;
  }
`;

class SearchBar extends Component {

    state = {
        isSearchBarBlurred : false,
        isMouseOnDropdown : false,
    };

    handleInputChange(e) {
        this.props.searchHashtags(e.target.value);
    }

    onSearchBarBlur() {
        this.setState({
            isSearchBarBlurred : true
        })
    }

    onSearchBarFocus() {
        this.setState({
            isSearchBarBlurred : false
        })
    }

    onMouseEnterOnDropdown() {
        this.setState({
            isMouseOnDropdown : true
        })
    }

    onMouseLeaveOnDropdown() {
        this.setState({
            isMouseOnDropdown : false
        })
    }

    render() {
        const {
            selectedHashtags,
            selectHashtag,
            searchHashtags,
            formData
        } = this.props;

        const {
            isSearchBarBlurred,
            isMouseOnDropdown
        } = this.state;

        return (
            <Form>
                <StyledInput
                    key="search"
                    type="text"
                    component="input"
                    name="search"
                    placeholder="Search tags..."
                    onChange={this.handleInputChange.bind(this)}
                    onBlur={this.onSearchBarBlur.bind(this)}
                    onFocus={this.onSearchBarFocus.bind(this)}
                    autoComplete="off"
                />
                {
                    (isMouseOnDropdown || isSearchBarBlurred === false) &&
                    selectedHashtags.length > 0 &&
                    <Dropdown
                        onhover={() => console.log('on hover.')}
                        onMouseEnter={this.onMouseEnterOnDropdown.bind(this)}
                        onMouseLeave={this.onMouseLeaveOnDropdown.bind(this)}
                    >
                        {selectedHashtags.map(tag =>
                            <Chip
                                key={tag._id}
                                onTagClick={(e) => {
                                    e.preventDefault();
                                    selectHashtag(tag);
                                    searchHashtags(formData);
                                }}
                            >
                                {`#${tag.tag}`}
                            </Chip>
                        )}
                    </Dropdown>
                }
            </Form>
        );
    }
}

export default compose(
    connect(
        state => ({
            formData: searchFormSelector(state, 'search'),
            hashtagsById: getHashtags(state),
            selectedHashtags: searchedHashtagsSelector(state)
        }),
        dispatch => ({
            searchHashtags: bindActionCreators(searchHashtags, dispatch),
            selectHashtag : bindActionCreators(selectHashtag, dispatch)
        })
    ),
    reduxForm({form: 'searchForm'})
)(SearchBar);
