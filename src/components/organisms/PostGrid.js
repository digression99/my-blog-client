import React from 'react';
import styled from 'styled-components';

import { GridItem } from '../molecules/index';

const Wrapper = styled.div`
  display : grid;
  grid-template-columns : repeat(3, 1fr);
  //grid-auto-rows: minmax(5rem, auto);
  width : 100%;
`;

const PostGrid = ({ posts, hashtags }) => {

    const renderPosts = () => {
        const postsJsx = [];
        for (let id in posts) {
            const tags = [];
            posts[id].hashtags.map(tagId => tags.push(hashtags[tagId]));

            postsJsx.push(<GridItem
                key={posts[id].id}
                {...posts[id]}
                tags={tags}
            />);
        }
        return postsJsx;
    };

    return (
        <Wrapper>
            {renderPosts()}
        </Wrapper>
    );
};

export default PostGrid;
