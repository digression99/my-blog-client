import React, {Component} from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

const Wrapper = styled.nav`
  display : flex;
  align-items : center;
  background : transparent;
  min-width : 800px;
`;

const NavLeft = styled.div`
    flex : 2 1 auto;
    margin-left : 1rem;
`;

const NavRight = styled.div`
    display : flex;
    flex : 4 1 auto;
    justify-content : flex-end;
    font-size : 1.5rem;
    margin-right : 1rem;
`;

const TitleLink = styled(Link)`
  text-decoration : none;
  font-size : 2rem;
  transition : all 0.2s ease-out;
  color : black;
  
  &:hover {
      color : red;
      //transform : scale(1.1);
    }
`;

const StyledLink = styled(Link)`
    text-decoration : none;
    font-weight : bold;
    font-size : 1.2rem;
    transition : all 0.2s ease-out;
    color : black;
    
    &:hover {
      color : red;
      //transform : scale(1.1);
    }
`;

const ListItem = styled.li`
    display : inline-flex;
    align-items : center;
`;

class Header extends Component {

    renderMenu() {
        return [
            <StyledLink to="/posts">posts</StyledLink>
        ];
    }

    render() {
        return (
            <Wrapper>
                <NavLeft>
                    <TitleLink to="/">
                        #
                    </TitleLink>
                </NavLeft>
                <NavRight>
                    {this.renderMenu().map((menu, idx) => {
                        return (
                            <ListItem key={idx}>
                                {menu}
                            </ListItem>
                        );
                    })}
                </NavRight>
            </Wrapper>
        );
    }
}

export default Header;