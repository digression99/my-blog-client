import React, { Fragment } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

const Wrapper = styled.div`
  .message-box {
    position : fixed;
    display : flex;
    justify-content : center;
    align-items : center;
    
    top : ${window.innerHeight / 4}px;
    left : 50%;
    transform : translate(-50%, -50%);
    width : 400px;
    height : 50px;
    background-color : red;
    color : white;
    border-radius : 100px;
  }
  
  .cover {
    content : "";
    position : fixed;
    top : 0;
    left : 0;
    width : ${window.innerWidth}px;
    height : ${window.innerHeight}px;
    background-color : rgba(50, 50, 50, 0.2);
  }
`;

const ProgressBar = ({ inProgress, message }) => {
    return (
        <Fragment>
            {inProgress &&
                <Wrapper>
                    <div className="cover" />
                    <div className="message-box">{message}</div>
                </Wrapper>
            }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    inProgress : state.ui.inProgress,
    message : state.ui.message
});

export default compose(
    connect(mapStateToProps, null)
)(ProgressBar);
