import React from 'react';
import styled from 'styled-components';

import {
    Button,
    MarkdownViewer,
    Link
} from '../atoms';

const PostDetail = styled.div`
  display : flex;
  flex-direction : column;
  
  & .post-title {
        flex : 4 1 auto;
        font-size : 2rem;
        color : ${props => {
    // console.log('props in styled : ', props);
    if (props.primary) {
        return 'red';
    } else {
        return 'green';
    }
}}
    }
    
    & .post-content {
        flex : 4 1 auto;
        font-size : 1rem;
        //background-color : #E8E6AA;
        background-color : transparent;
        padding : 1.5rem;
        min-height : 20rem;
    }
    
    & .post-date-created {
        flex : 1 1 auto;
        font-size : 0.8rem;
        color : gray;
    }
`;

const TitleBox = styled.div`
  display : flex;
  flex-direction : row;
  
  .title-side {
    flex : 7 1 auto;
  }
  
  .button-side {
    flex : 3 1 auto;
    display : flex;
    justify-content : flex-end;
  
  }
`;

export default ({
                    id,
                    title,
                    content,
                    dateCreated,
                    onDeleteButtonClick
                }) => {
    return (
        <PostDetail>
            <TitleBox>
                <div
                    className="title-side"
                >
                    <div
                        className="post-title"
                    >
                        {title}
                    </div>
                    <div
                        className="post-date-created"
                    >
                        {dateCreated}
                    </div>
                </div>
                <div
                    className="button-side"
                >
                    <Link to={`/posts/edit/${id}`}>Edit</Link>
                    <Button
                        onClick={onDeleteButtonClick}
                    >Delete</Button>
                </div>
            </TitleBox>
            <div
                className="post-content"
            >
                <MarkdownViewer source={content} />
            </div>
        </PostDetail>
    );
};