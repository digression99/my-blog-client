import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { selectedHashtagsSelector } from "../../modules/hashtag";
import {
    ChipBox,
    SearchBar
} from '../molecules';

const StyledHashBoxContainer = styled.div` 
  width : 100%;
  min-height : 5rem;
`;

const LeftSide = styled.div`
  flex : 1 1 auto;
`;

class HashBox extends Component {
    render() {
        return (
            <StyledHashBoxContainer>
                <SearchBar />
                <ChipBox
                    hashtags={this.props.hashtags}
                    isInBox={true}
                />
            </StyledHashBoxContainer>
        );
    }
}

const mapStateToProps = state => ({
    hashtags : selectedHashtagsSelector(state, "this is test")
});

export default connect(mapStateToProps, null)(HashBox);







