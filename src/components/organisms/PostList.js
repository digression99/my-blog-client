import React, { Component } from 'react';
import styled from 'styled-components';

import { ListItem } from '../molecules';

const Wrapper = styled.ul`
  display : flex;
  flex-direction : column;
  justify-content : center;
  align-items : center;
  
  flex : 1 1 auto;
  width : 100%;
  height : 100%;
`;

class PostList extends Component {

    renderPosts() {
        const { posts, hashtags } = this.props;
        return posts.map((post, idx) =>
            <ListItem
                bgColor={idx % 2 === 0 ? 'blanchedalmond' : 'gray'}
                hoverColor={idx % 2 === 0 ? 'gray' : 'blanchedalmond'}
                key={post.id}
                {...post}
                tags={post.hashtags.map(id => hashtags[id])}
            />
        );
    }

    render() {
        return (
            <Wrapper>
                {this.renderPosts()}
            </Wrapper>
        );
    }
}

export default PostList;