import React, { Component } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    display : flex;
    align-items : center;
    justify-content : center;
    
    background-color : transparent;
    font-size : 1rem;
    width : 100%;
    max-width : 800px;
`;

class Footer extends Component {
    render() {
        return (
            <Wrapper>
                <div>
                    Pseudocoder Kim
                </div>
            </Wrapper>
        );
    }
}

export default Footer;