import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { Link, Button } from '../atoms/index';
import { MarkdownForm } from '../molecules/index';

import {
    validatePostForm,
    renderFormErrorMessage,
    isEmptyObject
} from "../../lib/index";

const Form = styled.form`

  display : flex;
  flex-direction : column;
  
  .form-label {
    font-size : 1.5rem;
    margin : 0.5rem;
  }
  
  .form-text-input {
    line-height : 2rem;
    font-size : 1.5rem;
    
    flex : 1;
    
    &:focus {
      box-shadow: 10px 10px 5px #aaaaaa;
      background-color : lightgoldenrodyellow;
    }
  }
  
  .input-container {
    display : flex;
    border : 0.5rem solid rgba(0, 0, 0, 0.1);
    box-sizing : border-box;
    padding : 0.5rem;
  }
  
  .button-box {
    display : flex;
    justify-content : flex-end;
    padding : 0.3rem;
  }
  
  .form-button {
    display : flex;
    justify-content : center;
    align-items : center;
    margin-left : 0.5rem;
    border-radius : 20px;
  
    background-color : white;
    color : hotpink;
  
    width : 200px;
    height : 40px;
  
    font-size : 1.5rem;
    font-weight : bold;
    
    &:active {
      background-color : yellow;
    }
    &:hover {
      cursor : pointer;
    }
  }
`;

class PostForm extends Component {

    async handleSubmit(val) {
        const { history } = this.props;
        await this.props.onSubmit(val, history);
    }

    getInputSource() {
        const { formData } = this.props;

        if (formData && formData.values) {
            return formData.values.content;
        }
        return "";
    }

    componentDidMount() {
        console.log('scrolling....');
        window.scrollTo({
            'behavior': 'smooth',
            'left': 0,
            'top': 0
        });
    }

    render() {
        return (
                <Form
                    onSubmit={this.props.handleSubmit(async data => {
                        const error = validatePostForm(data);

                        if (!isEmptyObject(error)) {
                            renderFormErrorMessage(error);
                            return;
                        }
                        await this.handleSubmit(data);
                    })}
                >
                    <div
                        className="input-container"
                    >
                        <label
                            className="form-label"
                        >
                            title
                        </label>
                        <Field
                            key="title"
                            type="text"
                            component="input"
                            name="title"
                            placeholder="my title."
                            className="form-text-input"
                        />
                    </div>
                    <div
                        className="input-container"
                    >
                        <MarkdownForm
                            source={this.getInputSource()}
                        />
                    </div>
                    <div
                        className="input-container"
                    >
                        <label
                            className="form-label"
                        >
                            hashtags
                        </label>
                        <Field
                            key="hashtags"
                            type="text"
                            component="input"
                            name="hashtags"
                            placeholder="#hashtags, #myblog"
                            className="form-text-input"
                        />
                    </div>
                    <div
                        className="button-box"
                    >
                        <Link
                            to="/posts"
                            className="form-button"
                        >
                            Go Back
                        </Link>
                        <Button>
                            {this.props.pageName} Post
                        </Button>
                    </div>
                </Form>
        );
    }
}


export default compose(
    withRouter,
    connect(state => ({
        formData : state.form.postForm
    }), null),
    reduxForm({
        form : 'postForm',
    })
)(PostForm)

