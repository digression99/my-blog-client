import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

const StyledLink = styled(Link)`
    display : flex;
    justify-content : center;
    align-items : center;
    
    text-decoration : none;
    text-transform : uppercase;
    padding : 15px 40px;
    border-radius : 100px;
    
    &:link, &:visited {
        background-color: #fff;
        color : #777;
        font-size : 1.2rem;
        transition : all .2s ease-in-out;
    }

    &:hover {
        transform : translateY(-3px);
        box-shadow : 0 10px 20px rgba(0, 0, 0, .2); /*{offsetx, offsetdown, blur, color}*/
    }

    &:active { /* when clicked. */
        transform : translateY(-1px);
        box-shadow : 0 5px 10px rgba(0, 0, 0, .2);
    }
`;

export default ({ to, children }) => (
    <StyledLink
        to={to}
    >{children}
    </StyledLink>
);