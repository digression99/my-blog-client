import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
    display : inline-block;
    padding : 15px 40px;
    justify-content : center;
    align-items : center;
    
    text-decoration : none;
    text-transform : uppercase;
    margin-left : 0.5rem;
    border-radius : 100px;
    
    background-color : white;
    color : #777;
    
    font-size : 1.2rem;
    transition : all .2s ease-in-out;

    &:hover {
        cursor : pointer;
        transform : translateY(-3px);
        box-shadow : 0 10px 20px rgba(0, 0, 0, .2); // {offsetx, offsetdown, blur, color}
    }

    &:active {
        transform : translateY(-1px);
        box-shadow : 0 5px 10px rgba(0, 0, 0, .2);
    }
`;

const Button = ({ onClick, children }) => {
    return (
        <StyledButton
            onClick={onClick}
        >{children}
        </StyledButton>
    );
};

export default Button;
