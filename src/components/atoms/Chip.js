import React from 'react';
import styled from 'styled-components';
import {lighten} from 'polished';

const Chip = styled.li`
  position : relative;
  display : flex;
  flex-wrap : wrap;
  list-style-type: none;
  margin : 0.5rem;
  border-radius : 10px;
  background-color : ${lighten('0.4', '#00B7FF')};
  height : 2rem;
  min-width : 6.5rem;
  
  &:hover {
    .delete-chip, .add-chip {
      z-index : 10;
    }
  }
  
  .tag-name {
      font-size : 0.7rem;
      font-weight : bold;
      padding : 0.5rem;
      flex : 4 1 auto;
      cursor : pointer;
      background-color : transparent;
      box-sizing : border-box;
  }
  
  .hover-item {
    position : absolute;
    width : 100%;
    height : 100%;
    
    display : flex;
    justify-content : center;
    align-items : center;
    flex : 1 1 auto;
  }
  
  .delete-chip {
    display : flex;
    justify-content : center;
    align-items : center;
    width : inherit;
    height : inherit;
    border-radius : 10px;
    
    color : white;
    font-weight : bold;
    font-size : 1rem;
    
    background-color : rgba(0, 0, 0, 0.4);
    cursor : pointer;
    z-index : -1;
    box-sizing : border-box;
  }
  
  .add-chip {
    display : flex;
    justify-content : center;
    align-items : center;
    width : inherit;
    height : inherit;
    border-radius : 10px;
    
    color : white;
    font-size : 1rem;
    font-weight : bold;
    
    background-color : rgba(0, 0, 0, 0.4);
    cursor : pointer;
    z-index : -1;
    box-sizing : border-box;
  }
`;

export default ({
                    onTagClick,
                    hoverstate,
                    children,
                }) => {
    const renderHoverButton = (hoverstate, onClick) => {
        const className = hoverstate === "add" ? "add-chip" : "delete-chip";
        const content = hoverstate === "add" ? "Add" : "X";

        return (
            <button
                className={className}
                onClick={onClick}
            >
                {content}
            </button>
        );
    };

    return (
        <Chip>
            <button
                className="tag-name"
                onClick={onTagClick}
            >
                {children}
            </button>
            <div
                className="hover-item"
            >
                {renderHoverButton(hoverstate, onTagClick)}
            </div>
        </Chip>
    );
}
