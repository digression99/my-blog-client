import React from 'react';
import Markdown from 'react-markdown';
import styled from 'styled-components';

const Wrapper = styled(Markdown)`
    height : 100%;
    
    & * {
      word-break : break-all;
    }
    
    & h1 {
      font-size : 2rem;
    }
    
    & code {
      background-color : rgba(0, 0, 0, 0.1);
    }
    
    & li {
      margin-left : 1rem;
    }
    
    & hr {
      width : 100%;
      border-bottom : 2px dotted gray;
    }
`;

export default ({ source }) => (
    <Wrapper source={source}/>
)