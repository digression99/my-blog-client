import { formValueSelector } from 'redux-form';

export const searchFormSelector = formValueSelector('searchForm');

