import { schema } from 'normalizr';

export const hashtagSchema = new schema.Entity('hashtags', {}, {
    idAttribute : '_id'
});

export const postSchema = new schema.Entity('posts', {
    hashtags : [hashtagSchema]
});
export const arrayOfPosts = new schema.Array(postSchema);