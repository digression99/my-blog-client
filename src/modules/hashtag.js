import { createAction, handleActions } from 'redux-actions';
import { createSelector } from 'reselect';
import { combineReducers } from 'redux';

import {
    COMPLETE_FETCH_POSTS,
    SELECT_HASHTAG,
    DESELECT_HASHTAG,
    SEARCH_HASHTAGS
} from "./types";

export const selectHashtag = createAction(SELECT_HASHTAG);
export const deselectHashtag = createAction(DESELECT_HASHTAG);

export const getHashtags = state => state.hashtags.byId;
export const getHashtagsAllIds = state => state.hashtags.allIds;
export const getHashtagsSelected = state => state.hashtags.selected;

export const selectedHashtagsSelector = createSelector(
    getHashtags,
    getHashtagsSelected,
    (byId, selected) => {
        return selected.byId.map(id => byId[id]);
    }
);

export const searchHashtagsSelector = string => createSelector(
    getHashtags,
    getHashtagsAllIds,
    getHashtagsSelected,
    (byId, allIds, selected) => {
        const hashtags = allIds.map(id => byId[id]);
        const filtered = hashtags.filter(hashtag => hashtag.tag.includes(string));
        const filteredBySelected = filtered.filter(hashtag => selected.byTag.indexOf(hashtag.tag) === -1);

        const hash = {};

        for (let i = 0; i < filteredBySelected.length; ++i) {
            hash[filteredBySelected[i].tag] = filteredBySelected[i];
        }

        const ret = [];

        for (let dat in hash) {
            ret.push(hash[dat]);
        }
        return ret;
    }
);

const searchHashtagsAction = createAction(SEARCH_HASHTAGS);

export const searchHashtags = string => (dispatch, getState) => {
    if (!string.trim()) {
        dispatch(searchHashtagsAction([]));
        return;
    }

    const searched = searchHashtagsSelector(string)(getState());
    dispatch(searchHashtagsAction(searched));
};

const byId = handleActions({
    [COMPLETE_FETCH_POSTS] : (state, action) => {
        if (action.payload) {
            return { ...action.payload.entities.hashtags };
        }
        return state;
    }
}, {});

const allIds = handleActions({
    [COMPLETE_FETCH_POSTS] : (state, action) => {
        if (action.payload) {
            const postIds = action.payload.result;
            const posts = postIds.map(id => action.payload.entities.posts[id]);
            const hashtagIds = [];
            posts.map(post => {
                hashtagIds.push(...post.hashtags);
            });
            return hashtagIds;
        }
        return state;
    }
}, []);

const selectedById = handleActions({
    [SELECT_HASHTAG] : (state, action) => {
        if (state.includes(action.payload._id)) return state;
        return [...state, action.payload._id];
    },
    [DESELECT_HASHTAG] : (state, action) => state.filter(id => action.payload._id !== id)
}, []);

const selectedByTag = handleActions({
    [SELECT_HASHTAG] : (state, action) => {
        if (state.includes(action.payload.tag)) return state;
        return [ ...state, action.payload.tag ];
    },
    [DESELECT_HASHTAG] : (state, action) => state.filter(tag => action.payload.tag !== tag)
}, []);

export default combineReducers({
    byId,
    allIds,
    selected : combineReducers({
        byId : selectedById,
        byTag : selectedByTag
    })
});