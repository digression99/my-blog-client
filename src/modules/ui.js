import { createAction, handleActions, combineActions } from 'redux-actions';
import { createSelector } from 'reselect';

import {
    SHOW_GRID_POST,
    SHOW_LIST_POST,
    REQUEST_FETCH_POSTS,
    REQUEST_DELETE_POST,
    REQUEST_SUBMIT_CREATE_POST,
    COMPLETE_DELETE_POST,
    COMPLETE_FETCH_POSTS,
    COMPLETE_SUBMIT_CREATE_POST,
    OPEN_POST_DELETE_MODAL,
    CLOSE_POST_DELETE_MODAL,
    SELECT_POST,
    SEARCH_HASHTAGS
} from "./types";

// action creators.
export const showListPost = createAction(SHOW_LIST_POST);
export const showGridPost = createAction(SHOW_GRID_POST);
export const openPostDeleteModal = createAction(OPEN_POST_DELETE_MODAL);
export const closePostDeleteModal = createAction(CLOSE_POST_DELETE_MODAL);
export const selectPost = createAction(SELECT_POST);

// reducer initial state.
const uiInitialState = {
    postDisplay : 'list',
    inProgress : false,
    message : "",
    modalIsOpen : false,
    selectedPostId : "",
    searchedHashtags : []
};

const getSearchedHashtags = state => state.ui.searchedHashtags;

// selectors.
export const searchedHashtagsSelector = createSelector(
    getSearchedHashtags,
    tags => tags
);

// reducers.
export default handleActions({
    [SEARCH_HASHTAGS] :
        (state, action) => (({ ...state, searchedHashtags : action.payload })),
    [REQUEST_SUBMIT_CREATE_POST] :
        (state, action) => ({ ...state, inProgress : true, message : "creating post..."}),
    [REQUEST_FETCH_POSTS] :
        (state, action) => ({ ... state, inProgress : true, message : "fetching posts..."}),
    [REQUEST_DELETE_POST] :
        (state, action) => ({ ...state, inProgress : true, message : "deleting posts..."}),
    [COMPLETE_FETCH_POSTS] :
        (state, action) => ({ ...state, inProgress : false, message : "" }),
    [COMPLETE_SUBMIT_CREATE_POST] :
        (state, action) => ({ ...state, inProgress : false, message : "" }),
    [COMPLETE_DELETE_POST] :
        (state, action) => ({ ...state, inProgress : false, message : "" }),
    [SHOW_LIST_POST]:
        (state, action) => ({...state, postDisplay: 'list'}),
    [SHOW_GRID_POST]:
        (state, action) => ({...state, postDisplay: 'grid'}),
    [OPEN_POST_DELETE_MODAL] :
        (state, action) => ({ ...state, modalIsOpen : true }),
    [CLOSE_POST_DELETE_MODAL] :
        (state, action) => ({ ...state, modalIsOpen : false }),
    [SELECT_POST] :
        (state, action ) => ({ ...state, selectedPostId : action.payload})
}, uiInitialState);
