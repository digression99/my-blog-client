import axios from 'axios';
import { createAction, handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import { createSelector } from 'reselect';

import {
    normalizePosts,
    getTagsArray
} from "../lib";

import {
    REQUEST_FETCH_POSTS,
    COMPLETE_FETCH_POSTS,
    FAIL_FETCH_POSTS,

    REQUEST_SUBMIT_CREATE_POST,
    COMPLETE_SUBMIT_CREATE_POST,
    FAIL_SUBMIT_CREATE_POST,

    REQUEST_DELETE_POST,
    COMPLETE_DELETE_POST,
    FAIL_DELETE_POST,

    REQUEST_SUBMIT_EDIT_POST,
    COMPLETE_SUBMIT_EDIT_POST,
    FAIL_SUBMIT_EDIT_POST
} from './types';

import { getHashtagsSelected, getHashtags } from "./hashtag";

// action creators.
const requestFetchPosts = createAction(REQUEST_FETCH_POSTS);
const completeFetchPosts = createAction(COMPLETE_FETCH_POSTS, posts => posts);
const failFetchPosts = createAction(FAIL_FETCH_POSTS);

const requestSubmitCreatePost = createAction(REQUEST_SUBMIT_CREATE_POST);
const completeSubmitCreatePost = createAction(COMPLETE_SUBMIT_CREATE_POST);
const failSubmitCreatePost = createAction(FAIL_SUBMIT_CREATE_POST);

const requestDeletePost = createAction(REQUEST_DELETE_POST);
const completeDeletePost = createAction(COMPLETE_DELETE_POST);
const failDeletePost = createAction(FAIL_DELETE_POST);

export const deletePost = (id, history) => dispatch => new Promise(async (resolve, reject) => {
    dispatch(requestDeletePost());

    try {
        await axios.delete('/api/posts', { data : { postId : id } });
        dispatch(completeDeletePost(id));
        history.push('/posts');
        resolve();
    } catch (e) {
        dispatch(failDeletePost());
        reject(e);
    }
});

export const fetchPosts = () => dispatch => new Promise(async (resolve, reject) => {
    dispatch(requestFetchPosts());
    try {
        const res = await axios.get('/api/posts');
        const normalized = normalizePosts(res.data);
        dispatch(completeFetchPosts(normalized));
        resolve();
    } catch (e) {
        dispatch(failFetchPosts(e));
        reject(e);
    }
});

export const submitCreatePost = (post, history) => dispatch => new Promise(async (resolve, reject) => {
    dispatch(requestSubmitCreatePost());

    try {
        const payload = { ...post, tags : getTagsArray(post.hashtags) };
        await axios.post('/api/posts', payload);

        dispatch(completeSubmitCreatePost());
        history.push('/posts');
        window.scrollTo(0, 0);
        resolve();
    } catch (e) {
        dispatch(failSubmitCreatePost(e));
        reject(e);
    }
});

const requestSubmitEditPost = createAction(REQUEST_SUBMIT_EDIT_POST);
const completeSubmitEditPost = createAction(COMPLETE_SUBMIT_EDIT_POST);
const failSubmitEditPost = createAction(FAIL_SUBMIT_EDIT_POST);

export const submitEditPost = (postId, post, history) => dispatch => new Promise(async (resolve, reject) => {
    dispatch(requestSubmitEditPost());

    try {
        const payload = { ...post, tags : getTagsArray(post.hashtags), postId };
        const res = await axios.patch('/api/posts', payload);

        dispatch(completeSubmitEditPost());
        history.push('/posts');
        window.scrollTo(0, 0);
        resolve();
    } catch (e) {
        dispatch(failSubmitEditPost(e));
        reject(e);
    }
});

// selectors.
export const getPosts = state => {
    if (state.posts.allIds && state.posts.allIds.length > 0) {
        return state.posts.byId;
    }
    return null;
};

export const getSelectedPostId = state => state.ui.selectedPostId;

export const getPostById = createSelector(
    getPosts,
    getSelectedPostId,
    (posts, id) => {
        if (!posts || !id) return {};
        return posts[id];
    }
);

export const filterPostsByHashtagsSelector = createSelector(
    getPosts,
    getHashtagsSelected,
    getHashtags,
    (posts, selected, hashtagsById) => {
        if (!posts) return [];
        const postArray = Object.values(posts);
        if (selected.byId.length < 1) return postArray;

        return postArray.filter(post =>
                selected.byId.map(id =>
                    post.hashtags.map(id =>
                        hashtagsById[id].tag).includes(hashtagsById[id].tag))
                .reduce((acc, cur) => acc && cur));
    }
);

const byId = handleActions({
    [COMPLETE_FETCH_POSTS] : (state, action) => {
        if (action.payload) {
            return { ...action.payload.entities.posts };
        }
        return state;
    },
    [COMPLETE_DELETE_POST] : (state, action) => {
        return state;
    }
}, {});

const allIds = handleActions({
    [COMPLETE_FETCH_POSTS] : (state, action) => {
        if (action.payload) {
            return action.payload.result;
        }
        return state;
    },
    [COMPLETE_DELETE_POST] : (state, action) => {
        return state;
    }
}, []);

export default combineReducers({
    byId,
    allIds
}, {} );



