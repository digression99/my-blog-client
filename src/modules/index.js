import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import posts from './post';
import ui from './ui';
import hashtags from './hashtag';

export default combineReducers({
    posts,
    hashtags,
    ui,
    form : reduxForm
});