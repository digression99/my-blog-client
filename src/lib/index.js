import { normalize } from 'normalizr';
import { toast } from 'react-toastify';
import _ from 'lodash';

import { POST_FORM_FIELDS } from "../constants";
import { arrayOfPosts } from "../modules/models";

export const normalizePosts = dat => normalize(dat, arrayOfPosts);

export const validatePostForm = values => {
    const errors = {};

    if (!values.title) {
        errors.title = "no title";
    }

    if (!values.content) {
        errors.content = "no content";
    }

    if (!values.hashtags) {
        errors.hashtags = "no hashtags";
    }
    return errors;
};

export const renderFormErrorMessage = error => {
    POST_FORM_FIELDS.map(field => {
        error[field] && toast(`${field} : ${error[field]}`);
    });
};

export const isEmptyObject = obj => _.isEmpty(obj);

export const getTagsArray = hashtags => {
    const tags = hashtags.split('#');
    const filtered = tags.filter(tag => tag.length > 1);
    const trimmed = filtered.map(tag => _.trim(tag, '#,. '));
    return trimmed;
};

export const getTagsString = (tagIds, byId) => {
    const tags = tagIds.map(id => byId[id]);
    if (tags.length < 1) return "";
    return tags.map(tag => `#${tag.tag}`).reduce((l, r) => `${l}, ${r}`);
};