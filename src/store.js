import { createStore, applyMiddleware, compose } from 'redux';
import modules from './modules';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

const configureStore = initState => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(
        modules,
        initState,
        composeEnhancers(
            applyMiddleware(logger, thunk)
        ));
    // do extra things.
    return store;
};

export default configureStore;